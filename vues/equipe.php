<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>La chocolaterie - Equipe</title>

        <!--Global CSS-->
        <link rel="stylesheet" href="../style/global.css">
        <link rel="stylesheet" href="../style/dev-equipe.css">
    </head>
    <body>
        <?php include "layout/header.php"; ?>
            <main>
                <section class="bodyequipe">
                    <?php       
                        //connecter une base de donnée avec php //
                            $pdo = new PDO('sqlite:../db/chocolaterie.db');
                        // recuperer les resultats apartir de la table crée et on peut metre soit pdostatement soit n'importe quel nom//
                            $pdoStatement=$pdo->query('select * from equipe');
                        // afficher les resultats //
                            $result = $pdoStatement->fetchAll();
                        // organiser l'afichage du resultat'//
                    ?>
                    <?php foreach ($result as $value): ?>

                        <img  class="image" src=<?= $value["image"] ?> alt=<?= $value["alt"]?>/>
                        <p class="nom"><?=$value["NOM"].' '.$value["PRENOM"]?></p>
                        <article class="empLoi">
                        <h3 class="metier"><?=$value["EMLPOI"]?></h3>
                        <p class="contenue"><?=$value["CONTENUE"]?></p>
                        </article>
                    <?php endforeach; ?>
                </section>
            </main>  
        <?php include "layout/footer.php"; ?>
        <script src="../js/equipe.js"></script>
    </body>
</html>