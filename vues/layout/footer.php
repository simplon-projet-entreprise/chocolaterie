<hr/>

    <footer>

    <section>
        
        <article>
            <h2>Nous suivre</h2>
            <a href="https://www.facebook.com/SimplonChambery/" target="_blank">
                <img src="../medias/fb.png" alt="Facebook de la chocolaterie" title="Facebook de la chocolaterie"/>
            </a>
            <a href="https://twitter.com/ChamberySimplon" target="_blank">
                <img src="../medias/tw.png" alt="Twitter de la chocolaterie" title="Twitter de la chocolaterie"/>
            </a>
            
        </article>

    </section>

    </footer>

   