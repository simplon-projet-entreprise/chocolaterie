<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>La chocolaterie - Produits</title>

    <!--Global CSS-->
    <link rel="stylesheet" href="../style/global.css">
    <link rel="stylesheet" href="../style/produits.css">
    
</head>
<body>
    <?php include "layout/header.php"; ?>

    <main>
        <section class="shop-carousel">
            <img id="carouselImg" class="shop-carousel-pic" src="../medias/fb.png" width="100">
            <div class="carousel-wrapper">
                <header id="caourselTitle"></header>
                <p id="caourselDesc"></p>
            </div>
        </section>

        <section class="flexcontainer" id="shopProducts">
            <?php
                require "../db/config.php";
                require "../db/connectDB.php";
                $connect = new ConnectDB($config);
                $products = $connect->getProducts();
            ?>

            <?php foreach($products as $product): ?>
                <article
                    class="shop-element" itemscope itemtype="https://schema.org/Product"
                    data-name="<?= $product["name"] ?>"
                    data-image="<?= $product["image"] ?>"
                    data-description="<?= $product["description"] ?>"
                >
                    <header class="shop-element-title" itemprop="name">
                        <?= $product["name"] ?>
                    </header>
                    <img class="shop-element-pic" alt="" src="<?= $product["image"] ?>" itemprop="image"></img>
                    <p class="shop-element-desc" itemprop="description">
                        <?= $product["description"] ?>
                    </p>
                </article>
            <?php endforeach; ?>
        </section>
    </main>
    
    <?php include "layout/footer.php"; ?>
    <script src="../js/produits.js"></script>
</body>
</html>