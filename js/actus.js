//Récupere tous les artciles avec .actus comme classe
let articles = document.getElementsByClassName("actus")

//Récupère le nombre d'articles
let nbarticles = articles.length

//Parcours des articles pour l'ajout de l'évènement JS sur le click et le mouseover et le mouseou sur le titre de chaque article avec l'affichage du contenu dans un box
for(let i = 0; i < nbarticles ; i++)
{
    articles.item(i).querySelector("h3").addEventListener("click",function(){
        alert(this.nextElementSibling.innerHTML)
    })

    articles.item(i).querySelector("h3").addEventListener("mouseover",function(){
        this.style.cursor = "pointer"
        this.style.color = "brown"
    })

    articles.item(i).querySelector("h3").addEventListener("mouseout",function(){
        this.style.cursor = "none"
        this.style.color = "black"
    })
}
