INSERT INTO "actualites" VALUES(1,"Chocolats pour Pâques", "Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita velit accusamus, culpa incidunt maxime iste ex vitae minus necessitatibus tempora autem molestias optio similique reprehenderit est accusantium quisquam eaque quia.");
INSERT INTO "actualites" VALUES(2,'Chocolats pour Halloween', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur numquam commodi nostrum? Dignissimos illo hic facilis, beatae corporis unde neque blanditiis numquam iusto quo nam ducimus officiis? Eius, laboriosam consequuntur!');
INSERT INTO "actualites" VALUES(3,'Chocolats pour Noël', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nobis, tenetur consectetur cupiditate voluptates exercitationem quasi facere maxime eligendi necessitatibus quis dolorem ad nisi commodi voluptatum nostrum sit vitae voluptas itaque!');
INSERT INTO "actualites" VALUES(4,"Chocolats pour le jour de l'an", 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque similique magnam iure voluptate ut cum? Possimus architecto iure quod! Temporibus magnam, qui consequuntur sed soluta maxime nisi placeat. Natus, accusamus?');

INSERT INTO "products" VALUES(NULL, datetime('now'), "Macarons", "Goutez à nos macaron au chocolat !", "/medias/macaron-de-chocolat-d-isolement-sur-le-fond-blanc-68229572.jpg", 0);
INSERT INTO "products" VALUES(NULL, datetime('now'), "Truffes", "Laissez-vous tenter par nos truffes au chocolat au lait !", "/medias/truffes-au-chocolat-au-lait-facile.jpeg", 0);
INSERT INTO "products" VALUES(NULL, datetime('now'), 'Chocolat "Ruby"', 'Découvrez le chocolat "Ruby"!', "/medias/5bd9b3c8cd70fdc91b2ebad8.jpg", 0);



insert into "equipe"  values (1,"/medias/maitre.jpeg","image de Maitre chocolatier Aida","djoudi","aida","maitre chocolatier"
,"Elle  surveille les dosages de cacao, de sucre, de lait ou de crème,
amandes et noisettes qui sont ses matières premières et connaît parfaitement 
la réaction de ses produits à la chaleur. elle maîtrise les opérations de tempérage, trempage, enrobage et
les différentes techniques de cuisson.");
insert into "equipe"  values (2,"/medias/developpeur.jpeg","image de developpeur rachid","Ettabaai","rachid",
"developpeur"," Il est en charge du code et de la stabilité de la plateforme.");
insert into "equipe" values (3,"/medias/directeur.jpeg","image de directeur hossein","Md Delwar","hossein","directeur","il organise l’entreprise , il met en place des sous-systèmes, structures, méthodes et procédures nécessaires pour atteindre les objectifs.");
insert into "equipe" values (4,"/medias/designer.jpeg","image de designer lucas","van den hend" ,"lucas","designer","il s ‘occupe de l’originalité et la qualité du produit pour fidéliser une clientèle qui pourrait se tourner vers la chocolaterie industrielle, moins coûteuse. Face à une clientèle grandissante et de plus en plus exigeante en terme de qualité.");
