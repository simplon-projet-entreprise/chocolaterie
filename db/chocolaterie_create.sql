CREATE TABLE actualites (id INTEGER PRIMARY KEY, 
                         title VARCHAR(30), 
                         content TEXT);

CREATE TABLE products (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                       created_date TIME, 
                       name TEXT NOT NULL DEFAULT '', 
                       description TEXT NOT NULL DEFAULT '', 
                       image TEXT NOT NULL DEFAULT '', 
                       price DECIMAL(18, 2) NOT NULL DEFAULT 0);

CREATE TABLE contacts (id INTEGER PRIMARY KEY AUTOINCREMENT,
                       fullname VARCHAR(256),
                       email VARCHAR(256),
                       phonenumber VARCHAR(256),
                       content TEXT);

CREATE TABLE equipe     (id INTEGER PRIMARY KEY AUTOINCREMENT,image TEXT NOT NULL DEFAULT '',
                        alt VARCHAR(30),NOM VARCHAR(30),PRENOM VARCHAR(30),EMLPOI VARCHAR(30),
                        CONTENUE TEXT);
