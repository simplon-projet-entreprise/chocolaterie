<?php

$config = [
    "host_mysql" => "mysql:host=localhost; dbname=chocolaterie",
    "host_sqlite" => "sqlite:".dirname(__FILE__)."/chocolaterie.db",
    "username_mysql" => "root",
    "pwd_mysql" => "root"
];