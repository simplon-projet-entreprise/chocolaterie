#!/bin/bash
#usage: `sh reset.sh`

DBNAME="chocolaterie"


cd "$(dirname $0)"
echo moved to folder $(pwd)

echo "deleting existing db"
rm --verbose $DBNAME.db
rm --verbose $DBNAME.sqlite

echo
echo "executing .sql files"
for sqlfile in *.sql; do
	echo "executing file $sqlfile"
	cat "$sqlfile" | sqlite3 $DBNAME.db
done
echo "done"

echo
echo "tables:"
tables="$(sqlite3 $DBNAME.db ".tables")"

for table in $tables; do
	entriesNb=$(sqlite3 $DBNAME.db "SELECT count(*) FROM $table")
	echo table "$table" has $entriesNb entries
done

